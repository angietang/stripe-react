# stripe-react

run npm install

copy the environment variables file:

    cp .env.example .env

update `.env` with your own test Stripe API keys (https://dashboard.stripe.com/account/apikeys)

run npm start

go to localhost:3000

enter a payment and see it on your Stripe dashboard