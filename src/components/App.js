import '../../src/style.css'
import React, { Component } from 'react'
import { StripeProvider } from 'react-stripe-elements'
import MyStoreCheckout from './stripe/MyStoreCheckout'
class App extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <StripeProvider apiKey='pk_test_ExslAPVtTAfFx0pUAd2Fptin'>
        <MyStoreCheckout />
      </StripeProvider>
    )
  }
}

export default App; 