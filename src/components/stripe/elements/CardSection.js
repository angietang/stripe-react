import React, { Component } from 'react';
import {CardElement} from 'react-stripe-elements';

const style = {
  base: {
    iconColor: 'white',
    color: '#fff',
    fontWeight: 500,
    fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
    fontSize: '16px',
    fontSmoothing: 'antialiased',

    ':-webkit-autofill': {
      color: 'white',
    },
    '::placeholder': {
      color: '#c4f0ff',
    },
  },
  invalid: {
    iconColor: 'red',
    color: 'red',
  },
}
class CardSection extends Component {
  
  render() {
    return (
      <label>
        <CardElement style={style} />
      </label>
    );
  }
};

export default CardSection;