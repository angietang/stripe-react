import React from 'react'

const PersonalSection = props => (
  <div>
    <div className="row">
      <label>
        Name
      </label>
      <input 
          type="text" 
          name="name" 
          value={props.name} 
          onChange={props.handleChange} 
          placeholder="Jane Doe"
          required
        />
    </div>
    <div className="row">
      <label>
        Email
      </label>
      <input 
          type="text" 
          name ="email" 
          value={props.email} 
          onChange={props.handleChange} 
          placeholder="janedoe@gmail.com"
          required
        />
    </div>
    <div className="row">
      <label>
        Phone
      </label>
      <input 
          type="text" 
          name="phone" 
          value={props.phone} 
          onChange={props.handleChange} 
          placeholder="(941) 555-0123"
          required
        />
    </div>
    <div className="row">
      <label>
        Password
      </label>
      <input 
          type="password" 
          name="password" 
          value={props.password} 
          onChange={props.handleChange} 
          placeholder="***********"
          required
        />
    </div>
  </div>
)

export default PersonalSection;